//
//  AccountViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 27/1/2564 BE.
//

import UIKit
import LocalAuthentication

class AccountViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        accountView.tableView.register(AccountTableViewCell.self, forCellReuseIdentifier: "accountcell")
        accountView.tableView.delegate = self
        accountView.tableView.dataSource = self
        navigationItem.title = "Account"
        accountView.tableView.tableFooterView = UIView()
        accountView.tableView.backgroundColor = #colorLiteral(red: 0.8979414105, green: 0.8980956078, blue: 0.8979316354, alpha: 1)
        setView()
    }
    
    internal var accountView: AccountView = {
        let accountview = AccountView()
        accountview.translatesAutoresizingMaskIntoConstraints = false
        
        return accountview
        
    }()
    
    func setView() {
        view.addSubview(accountView)
        
        NSLayoutConstraint.activate([
            accountView.topAnchor.constraint(equalTo: view.topAnchor),
            accountView.leftAnchor.constraint(equalTo: view.leftAnchor),
            accountView.rightAnchor.constraint(equalTo: view.rightAnchor),
            accountView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
    }
    
    let data =  [["Name"], ["Favorites", "ChangeTheme"]]
    func authenticateUser() {
        let context = LAContext()
        var error: NSError?

        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify yourself!"

            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [unowned self] success, authenticationError in

                DispatchQueue.main.async {
                    if success {
                        //self.runSecretCode()
                    } else {
                        let ac = UIAlertController(title: "Authentication failed", message: "Sorry!", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                    }
                }
            }
        } else {
            let ac = UIAlertController(title: "Touch ID not available", message: "Your device is not configured for Touch ID.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    

}

extension AccountViewController: UITableViewDelegate,UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "accountcell", for: indexPath ) as! AccountTableViewCell
        cell.theme_backgroundColor = GlobalPicker.cellBackgroundColor
        if indexPath.section == 0 {
            cell.selectionStyle = .none
            cell.label.textAlignment = .center
        }else {
            cell.label.theme_textColor = GlobalPicker.cellTextColor
            cell.accessoryType = .disclosureIndicator
           // cell.tintColor = #colorLiteral(red: 0.001844842685, green: 0.4804738164, blue: 0.8750062585, alpha: 1)
        }
        
        cell.label.text = data[indexPath.section][indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 100
        }
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 && indexPath.row == 0 {
            self.navigationController?.pushViewController(FavoritesViewController(), animated: true)
        }else if indexPath.section == 1 && indexPath.row == 1 {
            self.navigationController?.pushViewController(VisualEffectViewController(), animated: true)
           // TabBarViewController().hidesBottomBarWhenPushed = true
//            let loginVC = PasscodeViewController()
//            loginVC.hidesBottomBarWhenPushed = true
//            loginVC.modalPresentationStyle = .overCurrentContext
//            present(loginVC, animated: true, completion: nil)
            //getBiometricType() == .touch
        }
        tableView.deselectRow(at: indexPath, animated: true)
       // authenticateUser()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " "
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    
    
}
