//
//  CreditViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 29/1/2564 BE.
//

import UIKit

class CreditViewController: UIViewController, UIGestureRecognizerDelegate {
    var hiddenSections = Set<Int>()
    internal var creditModel = CreditDB()
    var getcredit: Int?
    var tableViewData = [[CreditContent](),[CreditContent]()]
    
    init(getCredit: Int) {
        self.getcredit = getCredit
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        creditView.tableView.register(CreditTableViewCell.self, forCellReuseIdentifier: "creditcell")
        creditView.tableView.delegate = self
        creditView.tableView.dataSource = self
        creditView.tableView.estimatedRowHeight = 88
        creditView.tableView.rowHeight = UITableView.automaticDimension
        creditView.tableView.tableFooterView = UIView()
        navigationItem.title = "Credits"
        
        creditModel.getId(id: getcredit!)
        
        creditModel.handleManagementRequest { [self] (Content) in
            tableViewData[0] = (Content?.cast)!
            tableViewData[1] = (Content?.crew)!
           // creditView.tableView.reloadData()
        }
        
        setView()
    }
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        creditView.tableView.reloadData()
//    }
//
    internal var creditView: CreditView = {
        let credit = CreditView()
        credit.translatesAutoresizingMaskIntoConstraints = false
        
        return credit
    }()
    
    
    func setView() {
        view.addSubview(creditView)
        
        NSLayoutConstraint.activate([
            creditView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            creditView.leftAnchor.constraint(equalTo: view.leftAnchor),
            creditView.rightAnchor.constraint(equalTo: view.rightAnchor),
            creditView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
    }
    
    @objc func hideSection(sender: UIButton){
        let section = sender.tag
        
        func indexPathsForSection() -> [IndexPath] {
            var indexPaths = [IndexPath]()
            
            for row in 0..<1 {
                indexPaths.append(IndexPath(row: row,section: section))
            }
            print(" indexpath",indexPaths)
            return indexPaths
        }
        print(" indexPathsForSection ",indexPathsForSection())
        
        if self.hiddenSections.contains(section) {
            self.hiddenSections.remove(section)
            self.creditView.tableView.deleteRows(at: indexPathsForSection(),with: .fade)
        } else {
            self.hiddenSections.insert(section)
            self.creditView.tableView.insertRows(at: indexPathsForSection(),with: .bottom)
            self.creditView.tableView.reloadData()
         
             //self.creditView.tableView.reloadSections(IndexSet.init(integer: section), with: .fade)
        }
       
        
      //  self.creditView.tableView.reloadData()
    }
    
}

extension CreditViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.hiddenSections.contains(section) {
            return 1
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "creditcell", for: indexPath) as! CreditTableViewCell
        
        //cell.collectionView.reloadData()
        
        cell.collectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        cell.getcontent = self.tableViewData[indexPath.section]
        cell.heightFloatingConstraints.constant = cell.collectionView.contentSize.height
        print(indexPath," height ",cell.heightFloatingConstraints.constant)
//        cell.collectionView.setNeedsLayout();
//        cell.collectionView.layoutIfNeeded();
       
        //cell.frame.size.height = cell.collectionView.contentSize.height
        
        //tableView.rowHeight = cell.collectionView.contentSize.height
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return " Section "
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print(" indexPath ",indexPath)
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        // print(" screen ",UIScreen.main.bounds.height)
//        return view.safeAreaLayoutGuide.layoutFrame.height
//    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionButton = UIButton()
      
        
        
        if section == 0 {
            sectionButton.setTitle(String(" Cast "),for: .normal)
        }else {
            sectionButton.setTitle(String(" Crew "),for: .normal)
        }
        // sectionButton.setTitle(String(section),for: .normal)
        
        sectionButton.backgroundColor = .gray
        sectionButton.tag = section
        sectionButton.addTarget(self,action: #selector(self.hideSection(sender:)),for: .touchUpInside)
        return sectionButton
    }
    
    
}
