//
//  ExpandViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 21/1/2564 BE.
//

import UIKit

class ExpandViewController: UIViewController, UIScrollViewDelegate,ExpandViewProtocalDelegate {
    func didSelectReviews() {
        self.navigationController?.pushViewController(ReviewViewController(getMovieDetail: getMovieModel!), animated: true)
    }
    
    func didSelectTrailers() {
        print(" select Trailers ")
    }
    
    func didSelectCredits() {
        self.navigationController?.pushViewController(CreditViewController(getCredit: (getMovieModel?.id)!), animated: true)
    }
    
    func didSelectSimilars() {
        self.navigationController?.pushViewController(RegisterViewController(), animated: true)
    }
    
    
    var getMovieModel: MovieDetail?

    init(getMovieModel: MovieDetail) {
        self.getMovieModel = getMovieModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(expandView)
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
        self.setNavigationBar()
        
        expandView.movienamelbl.text = getMovieModel?.original_title
        expandView.detailMovie.text = getMovieModel?.overview
        expandView.percentLabel.text = "\(Float((getMovieModel?.vote_average)!))"
        expandView.progressLoading.progress = (Float((getMovieModel?.vote_average!)!) * 10)/100
        expandView.releaseDateLabel.text = "Release Date\n\n\(getMovieModel!.release_date!)"
        expandView.posterImage.setImage(url: "https://image.tmdb.org/t/p/w500"+(getMovieModel?.poster_path)!, placeholderImage: #imageLiteral(resourceName: "avatar"))
        expandView.backDropImage.setImage(url: "https://image.tmdb.org/t/p/w500"+(getMovieModel?.backdrop_path)!, placeholderImage: #imageLiteral(resourceName: "avatar"))
        expandView.delegate = self
        //expandView.scrollView.contentSize = CGSize(width: self.view.frame.size.width, height: UIScreen.main.bounds.height)
       
        //print(" expanview ",expandView.scrollView.frame.size)
        
        let Favorite = UserDefault.getFavorite()
//       // let getIndex = Favorite.firstIndex(where: { $0.id == (getMovieModel?.id)! })
//
//        let getFavorite = Favorite.filter { $0.id == getMovieModel?.id }
//        print(" getFavorite ",getFavorite)
        print(" getFavorite ",UserDefault.getFavorite().count)
        if Favorite.firstIndex(where: { $0.id == getMovieModel?.id }) != nil{
            navigationItem.rightBarButtonItems![1].tintColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        }else {
            navigationItem.rightBarButtonItems![1].tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
        
        NSLayoutConstraint.activate([
            expandView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            expandView.leftAnchor.constraint(equalTo: view.leftAnchor),
            expandView.rightAnchor.constraint(equalTo: view.rightAnchor),
            expandView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        view.addGestureRecognizer(tapGesture)
        
    }
    
    override func viewDidLayoutSubviews() { //รอทำเสร็จ
        expandView.backDropImage.diagonalImage()

 //       expandView.scrollView.contentSize.height = view.safeAreaInsets.bottom //view.frame.size.height
        
        if expandView.scrollView.contentSize.height <= view.frame.size.height {
           expandView.scrollView.contentSize.height = view.frame.size.height-((tabBarController?.tabBar.frame.size.height)!)+2
        }
        
//        print(" View ",view.frame.size)
//        print(" expanview size ",expandView.scrollView.contentSize)
//        print(" tabbar ",tabBarController?.tabBar.frame)
        
    }
    
    
    internal var expandView: ExpandView = {
        var expandView = ExpandView()
        expandView.translatesAutoresizingMaskIntoConstraints = false
        return expandView
    }()
    
    
    func setNavigationBar() {

        //let button1 = UIBarButtonItem(image: UIImage(named: "imagename"), style: .plain, target: self, action: Selector(("action"))) // action:#selector(Class.MethodName) for swift 3
        
        let share = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        //let FavoriteButton = UIBarButtonItem(image: UIImage(named: "Favorites"), style: .plain, target: self, action: #selector(Favorites(sender:)))
        let FavoriteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Favorites"),  style: .plain, target: self, action: #selector(Favorites))
       
       // self.navigationItem.rightBarButtonItem  = [FavoriteButton,ShareButton]
        navigationItem.rightBarButtonItems = [share,FavoriteButton]
        //navigationItem.setRightBarButton([FavoriteButton,share], animated: true)
        navigationItem.title = getMovieModel?.original_title
    }

//    @objc func Save () { // remove @objc for Swift 3
//        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
//    }
//    @objc func Remove () { // remove @objc for Swift 3
//        navigationItem.rightBarButtonItem?.tintColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
//    }
    

    
    @objc func Favorites(sender: UIButton){
    
        switch (sender.tintColor){
        
        case #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0):
            navigationItem.rightBarButtonItems![1].tintColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
            Add()
        
        default:
            navigationItem.rightBarButtonItems![1].tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            Remove()
        }
    }
    
    func Add(){
        UserDefault.saveFavorite(movieDetail: getMovieModel!)
    }
    func Remove(){
        UserDefault.removeFavorite(id: (getMovieModel?.id)!)
        print(" remove ")
    }
    
    @objc func shareTapped(sender: UIButton){
    
        guard let image = expandView.posterImage.image?.jpegData(compressionQuality: 0.8) else {
              print("No image found")
              return
          }
        
        let object:URL = URL(string: "https://www.google.com/")!

          let vc = UIActivityViewController(activityItems: [object,image], applicationActivities: [])
          vc.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
          present(vc, animated: true)
    }
    

}

extension ExpandViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addObservers()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeObservers()
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        guard let userInfo = notification.userInfo, let frame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue  else {
            return
        }
        let contentInset = UIEdgeInsets(top: 0, left: 0, bottom: frame.height, right: 0)
        expandView.scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        expandView.scrollView.contentInset = UIEdgeInsets.zero
    }
    
    func removeObservers() {
        NotificationCenter.default.removeObserver(self)
    }
}
