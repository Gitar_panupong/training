//
//  FavoritesViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 27/1/2564 BE.
//

import UIKit
import CollectionViewSlantedLayout
import SDWebImage

class FavoritesViewController: UIViewController {
    
    // var collectionViewLayout: CollectionViewSlantedLayout!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        favoritesView.collectionView.register(FavoritesCollectionViewCell.self, forCellWithReuseIdentifier: "accountcell")
        favoritesView.collectionView.delegate = self
        favoritesView.collectionView.dataSource = self
        
        navigationItem.title = "Favorites"
       // favoritesView.collectionView.reloadData()
        setView()
        favoritesView.collectionView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            self.favoritesView.collectionView.reloadData()
            self.favoritesView.collectionView.collectionViewLayout.invalidateLayout()
    }
    
//    override func viewDidAppear(_ animated: Bool) {
//            self.favoritesView.collectionView.reloadData()
//            self.favoritesView.collectionView.collectionViewLayout.invalidateLayout()
////            self.favoritesView.collectionViewLayout.isFirstCellExcluded = true
//    }
    
    internal var favoritesView: FavoritesView = {
        let favoritesView = FavoritesView()
        favoritesView.translatesAutoresizingMaskIntoConstraints = false
        favoritesView.collectionView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        return favoritesView
        
    }()
    
    func setView() {
        view.addSubview(favoritesView)
        
        NSLayoutConstraint.activate([
            favoritesView.topAnchor.constraint(equalTo: view.topAnchor),
            favoritesView.leftAnchor.constraint(equalTo: view.leftAnchor),
            favoritesView.rightAnchor.constraint(equalTo: view.rightAnchor),
            favoritesView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
    }
}

extension FavoritesViewController: UICollectionViewDelegate,UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UserDefault.getFavorite().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "accountcell", for: indexPath) as! FavoritesCollectionViewCell
        cell.nameMovie.text = UserDefault.getFavorite()[indexPath.row].title
     // cell.imageMovie.setImage(url: "https://image.tmdb.org/t/p/w500"+UserDefault.getFavorite()[indexPath.row].poster_path!, placeholderImage: #imageLiteral(resourceName: "BlackImage"))
        
        DispatchQueue.main.async {
            cell.imageMovie.sd_setImage(with: URL(string: "https://image.tmdb.org/t/p/w500\(UserDefault.getFavorite()[indexPath.row].poster_path!)"), placeholderImage: UIImage(named: "BlackImage.png"))
        }
      


        
        if let layout = collectionView.collectionViewLayout as? CollectionViewSlantedLayout {
         //   cell.contentView.transform = CGAffineTransform(rotationAngle: layout.slantingAngle)
            layout.isFirstCellExcluded = true
            layout.isLastCellExcluded = true
            layout.lineSpacing = 15
            layout.itemSize = 230
        }
        return cell
    }
    
}

extension FavoritesViewController: UIScrollViewDelegate,CollectionViewDelegateSlantedLayout,UICollectionViewDelegateFlowLayout {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let collectionView = favoritesView.collectionView
        guard let visibleCells = collectionView.visibleCells as? [FavoritesCollectionViewCell] else {return}
        for parallaxCell in visibleCells {
            let yOffset = (collectionView.contentOffset.y - parallaxCell.frame.origin.y) / parallaxCell.imageHeight
            let xOffset = (collectionView.contentOffset.x - parallaxCell.frame.origin.x) / parallaxCell.imageWidth
            parallaxCell.offset(CGPoint(x: xOffset * xOffsetSpeed, y: yOffset * yOffsetSpeed))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //NSLog("Did select item at indexPath: [\(indexPath.section)][\(indexPath.row)]")
        navigationController?.pushViewController(ExpandViewController(getMovieModel: UserDefault.getFavorite()[indexPath.row]), animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: CollectionViewSlantedLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGFloat {
        return collectionViewLayout.scrollDirection == .vertical ? 275 : 325
    }
    
}
