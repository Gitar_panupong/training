//
//  HomeViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 19/1/2564 BE.
//

import UIKit


class HomeViewController: UIViewController,HomeViewProtocalDelegate {
    
    func backTouchUp() {
        print("press back")
        //  self.dismiss(animated: true, completion: nil)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    internal var homeView: HomeView = {
        let homeview = HomeView()
        homeview.translatesAutoresizingMaskIntoConstraints = false
        homeview.backgroundColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        return homeview
    }()
    
    internal var demoModel = DemoData()
    var model = [ModelDemo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        homeView.tableView.delegate = self
        homeView.tableView.dataSource = self
        homeView.tableView.register(InitTableViewCell.self, forCellReuseIdentifier: "mycell")
        homeView.delegate = self
        view.addSubview(homeView)
        
        
        NSLayoutConstraint.activate([
            homeView.topAnchor.constraint(equalTo: view.topAnchor),
            homeView.leftAnchor.constraint(equalTo: view.leftAnchor),
            homeView.rightAnchor.constraint(equalTo: view.rightAnchor),
            homeView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        demoModel.handleManagementRequest { [self] (testDataArray) in
           
            model = testDataArray!
            print(testDataArray!.toJSONString(prettyPrint: true)!)
            homeView.tableView.reloadData()
        }
        
    }
}

extension HomeViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath as IndexPath) as! InitTableViewCell
      
        cell.profileImageView.image = #imageLiteral(resourceName: "fish")
       // cell.headerLabel.text = "\(indexPath.row)"
        cell.headerLabel.text = "\(model[indexPath.row].userId!)"
        cell.detailLabel.text = model[indexPath.row].title
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //tableView.deselectRow(at: indexPath, animated: true) //กระพริบๆ
    }
    
}
