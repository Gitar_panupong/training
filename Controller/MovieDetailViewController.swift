//
//  MovieDetailViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 21/1/2564 BE.
//

import UIKit

class MovieDetailViewController: UIViewController {
    
    //  internal var movieModel = MovieData()
    //  var getmovie = [MovieDemo]()
    
    internal var moviedbmodel = MovieDB()
    var getmovie = [MovieDetail]()
    var getresult = [MovieDetail]()
    let searchbar = UISearchBar()
    
    internal var movieDetailView: MovieDetailView = {
        var movieDetailView = MovieDetailView()
        movieDetailView.translatesAutoresizingMaskIntoConstraints = false
        movieDetailView.backgroundColor = #colorLiteral(red: 0.721568644, green: 0.8862745166, blue: 0.5921568871, alpha: 1)
        return movieDetailView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(movieDetailView)
        
        navigationItem.title = "Upcoming Movies"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(ShowSearchbar))
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        searchbar.sizeToFit()
        searchbar.delegate = self
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "exchange"), style: .plain, target: self, action: #selector(ShowSearchbar))
        
        movieDetailView.collectionView.delegate = self
        movieDetailView.collectionView.dataSource = self
        // movieDetailView.collectionView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        movieDetailView.collectionView.theme_backgroundColor = GlobalPicker.cellBackgroundColor
        movieDetailView.collectionView.register(MovieDetailCollectionView.self, forCellWithReuseIdentifier: "movieDetailCell")
        
        NSLayoutConstraint.activate([
            movieDetailView.topAnchor.constraint(equalTo: view.topAnchor),
            movieDetailView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            movieDetailView.leftAnchor.constraint(equalTo: view.leftAnchor),
            movieDetailView.rightAnchor.constraint(equalTo: view.rightAnchor)
        ])
        
        
        //        movieModel.handleManagementRequest { [self] (getFeed) in
        //            getmovie = getFeed!.feed!.result!
        //            movieDetailView.collectionView.reloadData()
        //
        //        }
        
        moviedbmodel.handleManagementRequest { [self] (Item) in
            getresult = Item!.items!
            getmovie = getresult
            movieDetailView.collectionView.reloadData()
        }
    }
    
    @objc func ShowSearchbar(){
        navigationItem.titleView = searchbar
        searchbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        searchbar.showsCancelButton = true
        searchbar.barTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        searchbar.becomeFirstResponder()
        navigationItem.rightBarButtonItem = nil
    }
    
}

extension MovieDetailViewController: UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getmovie.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieDetailCell", for: indexPath) as! MovieDetailCollectionView
        
        cell.nameLabel.text = getmovie[indexPath.row].title
        //let url = URL(string: "https://image.tmdb.org/t/p/w500"+getmovie[indexPath.row].poster_path!)
        
        
        
        
        //cell.imageMovie.downloaded(from: url!)
        cell.update(imageUrl: "https://image.tmdb.org/t/p/w500"+getmovie[indexPath.row].poster_path!)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        //  let cell = collectionView.cellForRowAtIndexPath(indexPath) as!  ExpandViewController
        
        //        let viewcontroller = ExpandViewController()
        //        viewcontroller.getMovieModel = getmovie[indexPath.row]
        //        self.navigationController?.pushViewController(ExpandViewController(), animated: true)
        
        // let viewcontroller = ExpandViewController(getMovieModel: getmovie[indexPath])
        self.navigationController?.pushViewController(ExpandViewController(getMovieModel: getmovie[indexPath.row]), animated: true)
        
        
        
        
        
    }
    
    
    //    func numberOfSections(in collectionView: UICollectionView) -> Int {
    //        return 10
    //    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 150);
    }
    
    
    
}

extension MovieDetailViewController: UISearchBarDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(ShowSearchbar))
        searchbar.showsCancelButton = false
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        getmovie = getresult.filter { (MovieDetail) -> Bool in
            return (MovieDetail.title?.hasPrefix(searchText))! //== searchText
        }
        //   getmovie = find
        movieDetailView.collectionView.reloadData()
        
    }
    
    
}
