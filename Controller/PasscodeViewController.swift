//
//  PasscodeViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 1/2/2564 BE.
//

import UIKit
import SmileLock

class PasscodeViewController: UIViewController {
    
    //var passwordContainerView: PasswordContainerView!
    let kPasswordDigit = 6
    var passwordUIValidation: MyPasswordUIValidation!

    override func viewDidLoad() {
        super.viewDidLoad()
//        passwordContainerView = PasswordContainerView.create(in: passcodeView.stackView, digit: kPasswordDigit)
//        passwordContainerView.delegate = self
//        passwordContainerView.deleteButtonLocalizedTitle = "smilelock_delete"
//
//        //customize password UI
//        passwordContainerView.tintColor = .green
//        passwordContainerView.highlightedColor = .blue
       // view.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "fish"))
        setupView()
        passwordUIValidation = MyPasswordUIValidation(in: passcodeView.passwordStackView)
        
        passwordUIValidation.success = { [weak self] _ in
            print("*️⃣ success!")
            self?.navigationController?.pushViewController(TabBarViewController(), animated: true)
            self?.dismiss(animated: true, completion: nil)
        }

        passwordUIValidation.failure = {
            //do not forget add [weak self] if the view controller become nil at some point during its lifetime
            //self.dismiss(animated: true, completion: nil)
            self.passcodeView.Label.text = "Try Again!"
            print("*️⃣ failure!")
        }
      
        //visual effect password UI
        passwordUIValidation.view.rearrangeForVisualEffectView(in: self)

        passwordUIValidation.view.deleteButtonLocalizedTitle = "delete"
       
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }

//    internal var blurView: UIVisualEffectView = {
//       let blurview = UIVisualEffectView()
//        blurview.translatesAutoresizingMaskIntoConstraints = false
//
//        return blurview
//    }()

    internal var passcodeView: PassCodeView = {
        let passcodeView = PassCodeView()
        passcodeView.translatesAutoresizingMaskIntoConstraints = false
        
        return passcodeView
    }()
    internal var blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.alpha = 1
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
        
        return blurEffectView
    }()
    
    internal var vibrancy: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let vibrancyView: UIVisualEffectView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
    
        vibrancyView.translatesAutoresizingMaskIntoConstraints = false
       
        return vibrancyView
    }()
    
    func setupView(){
        
        //blurView.addSubview(self.view)
        view.addSubview(blurEffectView)
        blurEffectView.contentView.addSubview(vibrancy)
        vibrancy.contentView.addSubview(passcodeView)
        
        NSLayoutConstraint.activate([
            blurEffectView.topAnchor.constraint(equalTo: view.topAnchor),
            blurEffectView.leftAnchor.constraint(equalTo: view.leftAnchor),
            blurEffectView.rightAnchor.constraint(equalTo: view.rightAnchor),
            blurEffectView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        ])
        NSLayoutConstraint.activate([
            vibrancy.topAnchor.constraint(equalTo: blurEffectView.contentView.topAnchor),
            vibrancy.leftAnchor.constraint(equalTo: blurEffectView.contentView.leftAnchor),
            vibrancy.rightAnchor.constraint(equalTo: blurEffectView.contentView.rightAnchor),
            vibrancy.bottomAnchor.constraint(equalTo: blurEffectView.contentView.bottomAnchor),
        ])
        
        NSLayoutConstraint.activate([
            passcodeView.topAnchor.constraint(equalTo: vibrancy.contentView.topAnchor),
            passcodeView.leftAnchor.constraint(equalTo: vibrancy.contentView.leftAnchor),
            passcodeView.rightAnchor.constraint(equalTo: vibrancy.contentView.rightAnchor),
            passcodeView.bottomAnchor.constraint(equalTo: vibrancy.contentView.bottomAnchor),
        ])
    }
}
