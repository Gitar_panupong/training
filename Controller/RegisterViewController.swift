//
//  RegisterViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 19/1/2564 BE.
//

import UIKit
import SkeletonView
import iProgressHUD

class RegisterViewController: UIViewController, iProgressHUDDelegete,RegisterProtocalDelegate {
    
    func didSetButton() {
        dismiss(animated: true, completion: nil)
    }
    
    internal var moviedbmodel = MovieDB()
    var getmovie = [MovieDetail]()
    var getresult = [MovieDetail]()
    internal var moviemodel = MovieData()
    
    
    internal var registerView: RegisterView = {
        let register = RegisterView()
        register.translatesAutoresizingMaskIntoConstraints = false
        register.backgroundColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        return register
    }()
    
    let searchbar = UISearchBar()

    override func viewDidLoad() {
        super.viewDidLoad()
      
        registerView.tableView.delegate = self
        registerView.tableView.dataSource = self
        
        registerView.delegate = self
        
        navigationItem.title = "Popular Movies"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(ShowSearchbar))
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.isTranslucent = false
        searchbar.sizeToFit()
        searchbar.delegate = self
        
        view.addSubview(registerView)
        registerView.tableView.register(RegisterTableViewCell.self, forCellReuseIdentifier: "moviecell")
        NSLayoutConstraint.activate([
            registerView.topAnchor.constraint(equalTo: view.topAnchor),
            registerView.leftAnchor.constraint(equalTo: view.leftAnchor),
            registerView.rightAnchor.constraint(equalTo: view.rightAnchor),
            registerView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        registerView.tableView.isSkeletonable = true
        registerView.isSkeletonable = true
        view.isSkeletonable = true
        
        view.showAnimatedGradientSkeleton()
        
//        moviemodel.handleManagementRequest { [self] (getFeed) in
//            getresult = getFeed!.feed!.result!
//            registerView.tableView.reloadData()
//            view.hideSkeleton()
//          //  registerView.tableView.dismissProgress()
//        }
        
        moviedbmodel.handleManagementRequest { [self] (Item) in
            getresult = Item!.items!
            getmovie = getresult
            registerView.tableView.reloadData()
            view.hideSkeleton(transition: .crossDissolve(0.25))
        }

        
        
      
    }
      
      func onShow(view: UIView) {
          print("onShow")
      }
      
      func onDismiss(view: UIView) {
          print("onDismiss")
      }
      
      func onTouch(view: UIView) {
          print("onTouch")
      }
    
    @objc func ShowSearchbar(){
        navigationItem.titleView = searchbar
        searchbar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        searchbar.showsCancelButton = true
        searchbar.barTintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        searchbar.becomeFirstResponder()
        navigationItem.rightBarButtonItem = nil
    }

}

extension RegisterViewController: SkeletonTableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // return getresult.count
        return getmovie.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "moviecell", for: indexPath) as! RegisterTableViewCell
        cell.theme_backgroundColor = GlobalPicker.cellBackgroundColor
        cell.nameLabel.text = getmovie[indexPath.row].title
        cell.releaseLabel.text = getmovie[indexPath.row].release_date
        cell.genreLabel.text = "\(getmovie[indexPath.row].vote_count!)"
        cell.percentLabel.text = "\(getmovie[indexPath.row].vote_average!)"
        cell.progressLoading.progress = (Float(getmovie[indexPath.row].vote_average!) * 10)/100
       // let url = URL(string: "https://image.tmdb.org/t/p/w500"+getmovie[indexPath.row].poster_path!)
        
    //    cell.movieImageView.setImage(url: "https://image.tmdb.org/t/p/w500"+getmovie[indexPath.row].poster_path!)
        cell.movieImageView.setImage(url: "https://image.tmdb.org/t/p/w500"+getmovie[indexPath.row].poster_path!, placeholderImage: #imageLiteral(resourceName: "default"))
//        let dataTask = URLSession.shared.dataTask(with: url!) { [weak self] (data, _, _) in
//            if let data = data {
//                DispatchQueue.main.async {
//
//                    cell.movieImageView.image = UIImage(data: data)
//                }
//            }
//        }
//
//
//
//        dataTask.resume()
        
        
//        let url = URL(string: getmovie[indexPath.row].!)
//
//        let dataTask = URLSession.shared.dataTask(with: url!) { [weak self] (data, _, _) in
//            if let data = data {
//                DispatchQueue.main.async {
//                    // Create Image and Update Image View
//                    cell.movieImageView.image = UIImage(data: data)
//                }
//            }
//        }
//        dataTask.resume()
        
        cell.hideSkeleton()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "moviecell"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.navigationController?.pushViewController(ExpandViewController(getMovieModel: getmovie[indexPath.row]), animated: true)
    }
    
}

extension RegisterViewController: UISearchBarDelegate{
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        navigationItem.titleView = nil
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(ShowSearchbar))
            searchbar.showsCancelButton = false
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
      
            getmovie = getresult.filter { (MovieDetail) -> Bool in
                return (MovieDetail.title?.hasPrefix(searchText))! //== searchText
            }
         //   getmovie = find
            registerView.tableView.reloadData()
        
    }
    
    
}

