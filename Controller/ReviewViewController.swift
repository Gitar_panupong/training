//
//  ReviewViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 26/1/2564 BE.
//

import UIKit
import SkeletonView

class ReviewViewController: UIViewController {
    
    var getMovieName: String?
    var getMovie: MovieDetail?
    internal var reviewModel = ReviewDB()
    var getcontent = [ReviewContent]()
    
    init(getMovieDetail: MovieDetail) {
        self.getMovie = getMovieDetail
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(reviewView)
        reviewView.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        reviewView.tableView.register(ReviewTableViewCell.self, forCellReuseIdentifier: "reviewCell")
        reviewView.tableView.delegate = self
        reviewView.tableView.dataSource = self
        
        navigationItem.title = "Reviews"
        
        
        reviewView.tableView.estimatedRowHeight = 88
        reviewView.tableView.rowHeight = UITableView.automaticDimension
        
        NSLayoutConstraint.activate([
            reviewView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            reviewView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            reviewView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            reviewView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
        reviewView.tableView.isSkeletonable = true
        reviewView.isSkeletonable = true
        view.isSkeletonable = true
        view.showAnimatedGradientSkeleton()
        
        reviewModel.getId(id:(getMovie?.id)!)
        
        reviewModel.handleManagementRequest { [self] (Result) in
            getcontent = Result!.results!
            
            reviewView.tableView.reloadData()
            view.hideSkeleton()
            
            
        }
        
        
    }
    
    
    internal var reviewView: ReviewView = {
        var reviewView = ReviewView()
        reviewView.translatesAutoresizingMaskIntoConstraints = false
        return reviewView
    }()
}

extension ReviewViewController: UITableViewDelegate,SkeletonTableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getcontent.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewCell", for: indexPath) as! ReviewTableViewCell
        cell.NameLabel.text = getcontent[indexPath.row].author
        cell.ReviewLabel.text = getcontent[indexPath.row].content
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
        cell.hideSkeleton()
        //        }
        return cell
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "reviewCell"
    }
    
    
}
