//
//  TabBarViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 21/1/2564 BE.
//

import UIKit

class TabBarViewController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
 //       self.navigationController?.pushViewController(PasscodeViewController(), animated: true)
//        let loginVC = PasscodeViewController()
//        loginVC.modalPresentationStyle = .overCurrentContext
//        present(loginVC, animated: true, completion: nil)
       //view.theme_backgroundColor = ["#EB4F38", "#F4C600", "#56ABE4", "#ECF0F1"]
        self.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
      //  var tabOne = RegisterViewController()
        
        let tabPopularMovies = UINavigationController(rootViewController: RegisterViewController())
         let tabPopularMoviesbarItem = UITabBarItem(title: "Popular Movies", image: UIImage(named: "Movie.png"), tag: 1)
         tabPopularMovies.tabBarItem = tabPopularMoviesbarItem

        // let tabTwo = MovieDetailViewController()
         let tabUpcomingMovies = UINavigationController(rootViewController: MovieDetailViewController())
         tabUpcomingMovies.tabBarItem = UITabBarItem(title: "Upcoming Movies", image: UIImage(named: "Video.png"), selectedImage: UIImage(named: "selectedImage.png"))

         let tabAccount = UINavigationController(rootViewController: AccountViewController())
         tabAccount.tabBarItem = UITabBarItem(title: "Account", image: UIImage(named: "account.png"), selectedImage: UIImage(named: "selectedImage.png"))

         self.viewControllers = [tabPopularMovies,tabUpcomingMovies,tabAccount]
        
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        let loginVC = PasscodeViewController()
//        loginVC.modalPresentationStyle = .overCurrentContext
//        present(loginVC, animated: true, completion: nil)
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
          
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
