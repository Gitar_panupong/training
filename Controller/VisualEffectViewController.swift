//
//  VisualEffectViewController.swift
//  Training
//
//  Created by Panupong Chaiyarut on 3/2/2564 BE.
//

import UIKit

class VisualEffectViewController: UIViewController,VisualEffectViewProtocalDelegate {
    func didSelectDarkMode() {
        updateNightSwitch()
    }
    
    
    func didSelectChangeTheme() {
        MyThemes.switchToNext()
    }
    
    @objc private func updateNightSwitch() {
        visualEffectView.nightSwitch.isOn = MyThemes.isNight()
    }
    
    @IBAction func changeNight(_ sender: UISwitch) {
        MyThemes.switchNight(isToNight: sender.isOn)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        visualEffectView.delegate = self
        setView()
    }
    
    internal var visualEffectView: VisualEffectView = {
        let visualeffectView = VisualEffectView()
        visualeffectView.translatesAutoresizingMaskIntoConstraints = false
        visualeffectView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return visualeffectView
    }()
    
    func setView(){
        view.addSubview(visualEffectView)
        NSLayoutConstraint.activate([
            visualEffectView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            visualEffectView.leftAnchor.constraint(equalTo: view.leftAnchor),
            visualEffectView.rightAnchor.constraint(equalTo: view.rightAnchor),
            visualEffectView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])


    }
    


}
