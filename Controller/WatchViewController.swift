//
//  WatchViewController.swift
//  Training
//
//  Created by Nuttapon.T8 on 27/1/2564 BE.
//

import UIKit
import CollectionViewSlantedLayout

class WatchViewController: UIViewController {
    
    internal var favoritesView: UICollectionView = {
        let slantedSayout = CollectionViewSlantedLayout()
        slantedSayout.isFirstCellExcluded = true
        slantedSayout.isLastCellExcluded = true
        let favoritesView = UICollectionView(frame: .zero, collectionViewLayout: slantedSayout)
        favoritesView.translatesAutoresizingMaskIntoConstraints = false
        
        return favoritesView
        
    }()
    
    func setView() {
        view.addSubview(favoritesView)
        
        NSLayoutConstraint.activate([
            favoritesView.topAnchor.constraint(equalTo: view.topAnchor),
            favoritesView.leftAnchor.constraint(equalTo: view.leftAnchor),
            favoritesView.rightAnchor.constraint(equalTo: view.rightAnchor),
            favoritesView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        favoritesView.register(FavoritesCollectionViewCell.self, forCellWithReuseIdentifier: "CELL")
        favoritesView.delegate = self
        favoritesView.dataSource = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favoritesView.reloadData()
        favoritesView.collectionViewLayout.invalidateLayout()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return UIStatusBarAnimation.slide
    }
    
    
}

extension WatchViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return UserDefault.getFavorite().count
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CELL", for: indexPath)
                            as? FavoritesCollectionViewCell else {
            fatalError()
        }

        cell.imageMovie.image = #imageLiteral(resourceName: "avatar")

        if let layout = collectionView.collectionViewLayout as? CollectionViewSlantedLayout {
            cell.contentView.transform = CGAffineTransform(rotationAngle: layout.slantingAngle)
        }

        return cell
    }
}

extension WatchViewController: CollectionViewDelegateSlantedLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        NSLog("Did select item at indexPath: [\(indexPath.section)][\(indexPath.row)]")
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: CollectionViewSlantedLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGFloat {
        return collectionViewLayout.scrollDirection == .vertical ? 275 : 325
    }
}

extension WatchViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let collectionView = favoritesView
        guard let visibleCells = collectionView.visibleCells as? [FavoritesCollectionViewCell] else {return}
        for parallaxCell in visibleCells {
            let yOffset = (collectionView.contentOffset.y - parallaxCell.frame.origin.y) / parallaxCell.imageHeight
            let xOffset = (collectionView.contentOffset.x - parallaxCell.frame.origin.x) / parallaxCell.imageWidth
            parallaxCell.offset(CGPoint(x: xOffset * xOffsetSpeed, y: yOffset * yOffsetSpeed))
        }
    }
}
