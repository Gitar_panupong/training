//
//  GlobalPicker.swift
//  Training
//
//  Created by Panupong Chaiyarut on 3/2/2564 BE.
//

import SwiftTheme

enum GlobalPicker {
    static let backgroundColor: ThemeColorPicker = ["#fff", "#fff", "#fff", "#292b38"]
    static let cellBackgroundColor: ThemeColorPicker = ["#fff","#fff","#fff","#606060"]
    static let cellTextColor: ThemeColorPicker = ["#22446c","#EB4F38",  "#2492db", "#ECF0F1"]
    
    static let textColor: ThemeColorPicker = ["#000", "#000", "#000", "#ECF0F1"]
    static let barTintSelectColors: ThemeColorPicker = ["#aef2e4","#960000","#002080","#2874A6"]
    static let barTintUnselectColors: ThemeColorPicker = ["#fff","#fff","#fff","#fff"]
    static let barTextColors = ["#FFF", "#000", "#FFF", "#FFF"]
    static let barTextColor = ThemeColorPicker.pickerWithColors(barTextColors)
    static let barTintColor: ThemeColorPicker = ["#EB4F38", "#F4C600", "#56ABE4", "#01040D"]
    static let tabBarTintColor: ThemeColorPicker = ["#22446c","#EB4F38",  "#2492db", "#292b38"]
    static let NavigationBarTintColor: ThemeColorPicker = ["#22446c","#EB4F38",  "#2492db", "#292b38"]
}

