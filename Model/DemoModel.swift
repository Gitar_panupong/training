//
//  DemoModel.swift
//  ExampleCode
//
//  Created by Nuttapon.T8 on 19/1/2564 BE.
//

import Alamofire
import ObjectMapper
// get data api
class DemoData: NSObject {
    var completionHandler: (_ demoModel: [ModelDemo]?) -> Void = { _ in }
    
    func handleManagementRequest(block: @escaping (_ demoModel: [ModelDemo]?) -> Void) {
        completionHandler = block
        fetchData()
    }
    
    func fetchData() {
        AF.request("https://jsonplaceholder.typicode.com/albums",method: .get).responseJSON { [self] (response) in
            let data = Mapper<ModelDemo>().mapArray(JSONArray: response.value as! [[String : Any]])
            print(data.toJSONString(prettyPrint: true)!)
            completionHandler(data)
        }
    }
}

struct ModelDemo: Mappable {
    init?(map: Map) {
            
    }
    
    mutating func mapping(map: Map) {
        userId <- map["userId"]
        id <- map["id"]
        title <- map["title"]
    }
    
    var userId: Int?
    var id: Int?
    var title: String?
}
