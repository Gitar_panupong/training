//
//  MovieModel.swift
//  Training
//
//  Created by Panupong Chaiyarut on 20/1/2564 BE.
//

import Foundation
import Alamofire
import ObjectMapper

/////TestDemo

//////testThree

////Testtesttttt

class MovieData: NSObject {
    var completionHandler: (_ demoModel: Feed?) -> Void = { _ in }
    
    func handleManagementRequest(block: @escaping (_ demoModel: Feed?) -> Void) {
        completionHandler = block
        fetchData()
    }
    
    func fetchData() {
        AF.request("https://rss.itunes.apple.com/api/v1/th/movies/top-movies/all/10/explicit.json",method: .get).responseJSON { [self] (response) in
            let data = Mapper<Feed>().map(JSON: response.value as! [String : Any])
            print(data!.toJSONString(prettyPrint: true)!)
            completionHandler(data)
        }
    }
}

struct MovieDemo: Mappable {
    init?(map: Map) {
            
    }
    mutating func mapping(map: Map) {
        artistName <- map["artistName"]
        id <- map["id"]
        releaseDate <- map["releaseDate"]
        name <- map["name"]
        kind <- map["kind"]
        copyright <- map["copyright"]
        artworkUrl100 <- map["artworkUrl100"]
        url <- map["url"]
    }
    
    var artistName: String?
    var id: String?
    var releaseDate: String?
    var name: String?
    var kind:String?
    var copyright: String?
    var artworkUrl100: String?
    var url: String?
}


struct Result: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        title <- map["title"]
        result <- map["results"]
        
    }
    var title: String?
    var result: [MovieDemo]?
}

struct Feed: Mappable {
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        feed <- map["feed"]
    }
    var feed: Result?
}



