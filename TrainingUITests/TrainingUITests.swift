//
//  TrainingUITests.swift
//  TrainingUITests
//
//  Created by Panupong Chaiyarut on 18/1/2564 BE.
//

import XCTest

class TrainingUITests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        let app = XCUIApplication()
        setupSnapshot(app)
        app.launch()
    }

    func testScreenshots() {
        
        let app = XCUIApplication()
        
        XCUIDevice.shared.orientation = .portrait
        
        let tabBarsQuery = XCUIApplication().tabBars
        
        // Home
        tabBarsQuery.buttons.element(boundBy: 0).tap()
        snapshot("0-Home")
        
        // Map
        tabBarsQuery.buttons.element(boundBy: 1).tap()
        //app.otherElements["eventlocation"].tap()
        snapshot("1-Map")
        
        
        // Twitter
        tabBarsQuery.buttons.element(boundBy: 2).tap()
        snapshot("2-Map", waitForLoadingIndicator: true)
        
    }
}
