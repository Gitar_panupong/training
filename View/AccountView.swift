//
//  Account.swift
//  Training
//
//  Created by Panupong Chaiyarut on 27/1/2564 BE.
//

import UIKit

class AccountView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        AccountComponetnt()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var tableView: UITableView = {
       let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    func AccountComponetnt() {
        addSubview(tableView)

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: topAnchor),
            tableView.leftAnchor.constraint(equalTo: leftAnchor),
            tableView.rightAnchor.constraint(equalTo: rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
       
    }


}
