//
//  AccountTableViewCell.swift
//  Training
//
//  Created by Panupong Chaiyarut on 27/1/2564 BE.
//

import UIKit

class AccountTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setCellComponent()
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        return label
    }()
    
    func setCellComponent(){
        contentView.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            label.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: 10),
            label.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -10),
            label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -10),
            label.heightAnchor.constraint(equalToConstant: 20)
        ])
        
        
        
    }
    
}
