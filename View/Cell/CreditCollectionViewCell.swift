//
//  CreditCollectionViewCell.swift
//  Training
//
//  Created by Panupong Chaiyarut on 29/1/2564 BE.
//

import UIKit

class CreditCollectionViewCell: UICollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        contentView.addSubview(imageMovie)
        contentView.addSubview(nameLabel)
        
        NSLayoutConstraint.activate([
            imageMovie.topAnchor.constraint(equalTo: topAnchor),
            imageMovie.leftAnchor.constraint(equalTo: leftAnchor),
            imageMovie.rightAnchor.constraint(equalTo: rightAnchor),
            imageMovie.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            nameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            nameLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            nameLabel.heightAnchor.constraint(equalToConstant: 20),
            nameLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        
     
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(imageUrl: String) {
        imageMovie.setImage(url: imageUrl, placeholderImage: #imageLiteral(resourceName: "avatar"))
    }
    
    internal var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 10)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var imageMovie: UIImageView = {
       let imagemovie = UIImageView()
        imagemovie.image = #imageLiteral(resourceName: "avatar")
      //  imagemovie.image = #imageLiteral(resourceName: "avatar")
        imagemovie.translatesAutoresizingMaskIntoConstraints = false
        imagemovie.alpha = 0.4
     //   imagemovie.alpha = 0.3
        return imagemovie
    }()
}
