//
//  CreditTableViewCell.swift
//  Training
//
//  Created by Panupong Chaiyarut on 29/1/2564 BE.
//

import UIKit

class CreditTableViewCell: UITableViewCell {
    var getcontent = [CreditContent]()
    var isselect: IndexPath?
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        collectionView.register(CreditCollectionViewCell.self, forCellWithReuseIdentifier: "creditcollectioncell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        setCellComponent()
        
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .center
        return label
    }()
    
    internal var collectionView: UICollectionView = {
        let layout =  UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 20
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 2.5, bottom: 0, right: 2.5)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout) //
        
        let size = (collectionView.frame.size.width - CGFloat(30)) / CGFloat(3)
        layout.itemSize = CGSize(width: size, height: size)
        collectionView.setCollectionViewLayout(layout, animated: true)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    internal lazy var heightFloatingConstraints: NSLayoutConstraint = {
       
        return collectionView.heightAnchor.constraint(equalToConstant: 0)
    }()
    
    func setCellComponent(){
        contentView.addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: contentView.topAnchor),
            collectionView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            heightFloatingConstraints
        ])
        
    }
    
}

extension CreditTableViewCell: UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return getcontent.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "creditcollectioncell", for: indexPath) as! CreditCollectionViewCell
        
        // DispatchQueue.main.async { [self] in
        cell.nameLabel.text = "\(getcontent[indexPath.row].name!)\n\n\(getcontent[indexPath.row].known_for_department!)"
        
        cell.imageMovie.sd_setImage(with: URL(string:"https://image.tmdb.org/t/p/w500\(getcontent[indexPath.row].profile_path ?? "")"), placeholderImage: #imageLiteral(resourceName: "avatar"))
         // }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 110, height: 150);
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CreditCollectionViewCell
        
        if isselect != indexPath {
            if isselect != nil{
                let cell2 = collectionView.cellForItem(at: isselect!) as! CreditCollectionViewCell
                cell2.nameLabel.isHidden  = false
                cell2.imageMovie.alpha = 0.3
            }
            isselect = indexPath
            cell.nameLabel.isHidden = true
            cell.imageMovie.alpha = 1
        }else {
            isselect = nil
            cell.nameLabel.isHidden  = false
            cell.imageMovie.alpha = 0.3
        }
    }
    
    
    
    
    
}
