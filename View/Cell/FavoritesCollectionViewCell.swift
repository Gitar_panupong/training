//
//  CustomCollectionCell.swift
//  CollectionViewSlantedLayout
//
//  Created by Yassir Barchi on 28/02/2016.
//  Copyright © 2016-present Yassir Barchi. All rights reserved.
//

import Foundation

import UIKit

import CollectionViewSlantedLayout

let yOffsetSpeed: CGFloat = 100.0
let xOffsetSpeed: CGFloat = 100.0

class FavoritesCollectionViewCell: CollectionViewSlantedCell {

//    @IBOutlet weak var imageView: UIImageView!
    private var gradient = CAGradientLayer()
    
    override func awakeFromNib() {
        super.awakeFromNib()

        if let backgroundView = backgroundView {
            gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
            gradient.locations = [0.0, 1.0]
            gradient.frame = backgroundView.bounds
            backgroundView.layer.addSublayer(gradient)
        }
    }


    override func layoutSubviews() {
        super.layoutSubviews()
        if let backgroundView = backgroundView {
            gradient.frame = backgroundView.bounds
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
        contentView.addSubview(imageMovie)
        contentView.addSubview(nameMovie)
        
        NSLayoutConstraint.activate([
            imageMovie.topAnchor.constraint(equalTo: contentView.topAnchor),
            imageMovie.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            imageMovie.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            imageMovie.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            nameMovie.topAnchor.constraint(equalTo: contentView.topAnchor),
            nameMovie.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            nameMovie.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            nameMovie.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var imageMovie: UIImageView = {
       let imagemovie = UIImageView()
      //  imagemovie.image = #imageLiteral(resourceName: "avatar")
        imagemovie.translatesAutoresizingMaskIntoConstraints = false
        imagemovie.alpha = 0.4
        return imagemovie
    }()
    
    internal var nameMovie: UILabel = {
       let nameMovie = UILabel()
      //  imagemovie.image = #imageLiteral(resourceName: "avatar")
        nameMovie.translatesAutoresizingMaskIntoConstraints = false
        nameMovie.textColor = .white
        nameMovie.textAlignment = .center
        nameMovie.font = UIFont.boldSystemFont(ofSize: 16)
        
        return nameMovie
    }()

//    var image: UIImage = UIImage() {
//        didSet {
//            imageView.image = image
//        }
//    }
//
    var imageHeight: CGFloat {
        return (imageMovie.image?.size.height) ?? 0.0
    }

    var imageWidth: CGFloat {
        return (imageMovie.image?.size.width) ?? 0.0
    }

    func offset(_ offset: CGPoint) {
        imageMovie.frame = imageMovie.bounds.offsetBy(dx: offset.x, dy: offset.y)
    }
}
