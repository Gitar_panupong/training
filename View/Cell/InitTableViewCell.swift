//
//  InitTableViewCell.swift
//  Training
//
//  Created by Panupong Chaiyarut on 19/1/2564 BE.
//

import UIKit

class InitTableViewCell: UITableViewCell {
    
    
    internal var headerLabel: UILabel = {
       let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var detailLabel: UILabel = {
       let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var profileImageView: UIImageView = {
       let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
       
        //circularImageView.layer.cornerRadius = circularImageView.bounds.height / 2
       // imageView.image
        
        return imageView
    }()

    override func layoutSubviews() {
        super.layoutSubviews()
        profileImageView.layer.cornerRadius = 12
        profileImageView.clipsToBounds = true
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0))
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        contentView.addSubview(headerLabel)
        contentView.addSubview(profileImageView)
        contentView.addSubview(detailLabel)
        
        NSLayoutConstraint.activate([
            profileImageView.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            profileImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            profileImageView.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -10),
            profileImageView.widthAnchor.constraint(equalToConstant: 70)
        ])
        
        NSLayoutConstraint.activate([
            headerLabel.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -10),
           // detailLabel.topAnchor.constraint(equalTo: topAnchor, constant: ),
            headerLabel.rightAnchor.constraint(equalTo: rightAnchor),
            headerLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: -10)
            
            //headerLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
        ])
        
        NSLayoutConstraint.activate([
            detailLabel.topAnchor.constraint(equalTo: headerLabel.bottomAnchor, constant: 5),
            detailLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
            detailLabel.rightAnchor.constraint(equalTo: rightAnchor),
            detailLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 10)
            
            //headerLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -10),
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    



}
