//
//  MovieDetailTableViewCell.swift
//  Training
//
//  Created by Panupong Chaiyarut on 21/1/2564 BE.
//

import UIKit

class MovieDetailCollectionView: UICollectionViewCell {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.addSubview(imageMovie)
        contentView.addSubview(nameLabel)
        
        NSLayoutConstraint.activate([
            imageMovie.topAnchor.constraint(equalTo: topAnchor),
            imageMovie.leftAnchor.constraint(equalTo: leftAnchor),
            imageMovie.rightAnchor.constraint(equalTo: rightAnchor),
            imageMovie.bottomAnchor.constraint(equalTo: nameLabel.topAnchor)
        ])
        
        NSLayoutConstraint.activate([
          //  nameLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            nameLabel.leftAnchor.constraint(equalTo: leftAnchor),
            nameLabel.rightAnchor.constraint(equalTo: rightAnchor),
            nameLabel.heightAnchor.constraint(equalToConstant: 20),
            nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
     
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func update(imageUrl: String) {
        imageMovie.setImage(url: imageUrl, placeholderImage: #imageLiteral(resourceName: "avatar"))
    }
    
    internal var nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        label.sizeToFit()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var imageMovie: UIImageView = {
       let imagemovie = UIImageView()
        imagemovie.image = #imageLiteral(resourceName: "avatar")
      //  imagemovie.image = #imageLiteral(resourceName: "avatar")
        imagemovie.translatesAutoresizingMaskIntoConstraints = false
        
        return imagemovie
    }()
    
}
