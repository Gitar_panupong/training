//
//  RegisterTableViewCell.swift
//  Training
//
//  Created by Panupong Chaiyarut on 19/1/2564 BE.
//

import UIKit
import SkeletonView

class RegisterTableViewCell: UITableViewCell {
    

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    
        selectionStyle = .none
        
        contentView.addSubview(movieImageView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(genreLabel)
        contentView.addSubview(releaseLabel)
        contentView.addSubview(progressLoading)
        contentView.addSubview(percentLabel)
        
        self.isSkeletonable = true
        contentView.isSkeletonable = true
        movieImageView.isSkeletonable = true
        nameLabel.isSkeletonable = true
        genreLabel.isSkeletonable = true
        releaseLabel.isSkeletonable = true
        progressLoading.isSkeletonable = true
        percentLabel.isSkeletonable = true
        
        NSLayoutConstraint.activate([
            movieImageView.leftAnchor.constraint(equalTo: leftAnchor, constant: 10),
            movieImageView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            movieImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            movieImageView.widthAnchor.constraint(equalToConstant: 90)
        ])
        
        NSLayoutConstraint.activate([
            nameLabel.leftAnchor.constraint(equalTo: movieImageView.rightAnchor,constant: 10),
            nameLabel.rightAnchor.constraint(equalTo: progressLoading.leftAnchor),
            nameLabel.bottomAnchor.constraint(equalTo: genreLabel.topAnchor, constant: -20)
        ])
        NSLayoutConstraint.activate([
            genreLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            genreLabel.leftAnchor.constraint(equalTo: movieImageView.rightAnchor,constant: 10),
            genreLabel.rightAnchor.constraint(equalTo: progressLoading.leftAnchor)
        ])
        
        NSLayoutConstraint.activate([
            releaseLabel.topAnchor.constraint(equalTo: genreLabel.bottomAnchor, constant: 20),
            releaseLabel.leftAnchor.constraint(equalTo: movieImageView.rightAnchor,constant: 10),
            releaseLabel.rightAnchor.constraint(equalTo: progressLoading.leftAnchor)
        
        ])
        NSLayoutConstraint.activate([
            progressLoading.centerYAnchor.constraint(equalTo: centerYAnchor),
            progressLoading.rightAnchor.constraint(equalTo: rightAnchor,constant: -10),
            progressLoading.heightAnchor.constraint(equalToConstant: 50),
            progressLoading.widthAnchor.constraint(equalToConstant: 50)
        ])
        NSLayoutConstraint.activate([
            percentLabel.centerYAnchor.constraint(equalTo: centerYAnchor),
            percentLabel.leftAnchor.constraint(equalTo: progressLoading.leftAnchor),
            percentLabel.rightAnchor.constraint(equalTo: progressLoading.rightAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
        
    }()
    
    internal var genreLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var releaseLabel: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var movieImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    internal var progressLoading: CircularProgressView = {
        let progressLoading = CircularProgressView()
        
       // progressLoading.progress = 0.8
        
        progressLoading.trackLineWidth = 4.0
        progressLoading.trackTintColor = UIColor.lightGray
        progressLoading.progressTintColor = #colorLiteral(red: 0, green: 0.4793452024, blue: 0.9990863204, alpha: 1)
        progressLoading.roundedProgressLineCap = true
        
        progressLoading.translatesAutoresizingMaskIntoConstraints = false
        return progressLoading
    }()
    
    internal var percentLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    
    
}
