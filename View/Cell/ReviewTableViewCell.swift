//
//  ReviewTableViewCell.swift
//  Training
//
//  Created by Panupong Chaiyarut on 26/1/2564 BE.
//

import UIKit
import SkeletonView

class ReviewTableViewCell: UITableViewCell {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
     
        cellComponent()
        selectionStyle = .none
        self.isSkeletonable = true
        contentView.isSkeletonable = true
        NameLabel.isSkeletonable = true
        ReviewLabel.isSkeletonable = true
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var NameLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: 20)
        return label
    }()
    
    internal var ReviewLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .black
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.sizeToFit()
        return label
    }()
    
    
    func cellComponent() {
        contentView.addSubview(NameLabel)
        contentView.addSubview(ReviewLabel)
        
      //  let marginGuide = contentView.layoutMarginsGuide
        
        NSLayoutConstraint.activate([
            NameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
            NameLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: 10),
            NameLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -10),
            NameLabel.heightAnchor.constraint(equalToConstant: 20)
        ])
        NSLayoutConstraint.activate([
            ReviewLabel.topAnchor.constraint(equalTo: NameLabel.bottomAnchor,constant: 10),
            ReviewLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor,constant: 5),
            ReviewLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: 5),
            ReviewLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -10)
        ])
        
        
    }
    
}
