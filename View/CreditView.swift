//
//  CreditView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 29/1/2564 BE.
//

import UIKit

class CreditView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        CreditComponent()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        // tableView.isScrollEnabled = false
        tableView.contentInsetAdjustmentBehavior = .never
        tableView.insetsContentViewsToSafeArea = true
        
        
        return tableView
    }()
    
    func CreditComponent() {
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            // tableView.widthAnchor.constraint(equalTo: widthAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor)
        ])
        
    }
    
}
