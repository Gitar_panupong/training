//
//  ExpandView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 21/1/2564 BE.
//

import UIKit

protocol ExpandViewProtocalDelegate {
    func didSelectReviews()
    func didSelectTrailers()
    func didSelectCredits()
    func didSelectSimilars()
}

class ExpandView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        ExpandViewCoponent()
        let tapSelectReview = UITapGestureRecognizer(target: self, action: #selector(TappedReviews(tapGestureRecognizer:)))
        reviewImage.addGestureRecognizer(tapSelectReview)
        let tapSelectTrailers = UITapGestureRecognizer(target: self, action: #selector(TappedTrailers(tapGestureRecognizer:)))
        trailerImage.addGestureRecognizer(tapSelectTrailers)
        let tapSelectCredits = UITapGestureRecognizer(target: self, action: #selector(TappedCredits(tapGestureRecognizer:)))
        creditImage.addGestureRecognizer(tapSelectCredits)
        let tapSelectSimilarss = UITapGestureRecognizer(target: self, action: #selector(TappedSimilars(tapGestureRecognizer:)))
        similarImage.addGestureRecognizer(tapSelectSimilarss)
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        // backDropImage.diagonalImage()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var backDropImage: UIImageView = {
        var backDropImage = UIImageView()
        backDropImage.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        backDropImage.contentMode = .scaleAspectFill
        backDropImage.translatesAutoresizingMaskIntoConstraints = false
        return backDropImage
    }()
    
    internal var posterImage: UIImageView = {
        var posterImage = UIImageView()
        posterImage.backgroundColor = #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1)
        posterImage.translatesAutoresizingMaskIntoConstraints = false
        
        return posterImage
    }()
    
    
    internal var movienamelbl: UILabel = {
        let label = UILabel()
        label.text = "The Avengers"
        // label.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        label.textColor = .white
        label.textAlignment = .left
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var progressLoading: CircularProgressView = {
        let progressLoading = CircularProgressView()
        
        // progressLoading.progress = 0.8
        // progressLoading.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        progressLoading.trackLineWidth = 4.0
        progressLoading.trackTintColor = UIColor.lightGray
        progressLoading.progressTintColor = #colorLiteral(red: 0, green: 0.4793452024, blue: 0.9990863204, alpha: 1)
        progressLoading.roundedProgressLineCap = true
        
        
        progressLoading.translatesAutoresizingMaskIntoConstraints = false
        return progressLoading
    }()
    
    internal var percentLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        // label.font = UIFont.systemFont(ofSize: 10)
        label.text = "7.5"
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var releaseLabel: UILabel = {
        let label = UILabel()
        label.text = "When an unexpected enemy emerges and threatens global safety and security, Nick Fury, director of the international peacekeeping agency known as S.H.I.E.L.D., finds himself in need of a team to pull the world back from the brink of disaster. Spanning the globe, a daring recruitment effort begins!"
        label.textColor = .cyan
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var releaseLabel2: UILabel = {
        let label = UILabel()
        label.text = "Trailers"
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    internal var releaseLabel3: UILabel = {
        let label = UILabel()
        label.text = "Credits"
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    internal var releaseLabel4: UILabel = {
        let label = UILabel()
        label.text = "Similars"
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var stackView: UIStackView = {
        var stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.alignment = .center
        stackView.spacing = 15.0
        stackView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        stackView.layer.borderWidth = 0.5
        stackView.distribution = .fillEqually
        
        // stackView.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        return stackView
        
    }()
    
    internal var releaseDateLabel: UILabel = {
        let label = UILabel()
        label.text = "Release Date \n\n 2018-01-01"
        label.numberOfLines = 0
        label.textColor = .white
        label.sizeToFit()
        
        label.textAlignment = .center
        
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var detailMovie: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.backgroundColor = .clear
        label.sizeToFit()
        label.font = UIFont.systemFont(ofSize: 16)
        //    textView.isEditable = false
        //label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var button: UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 0.4666666687, green: 0.7647058964, blue: 0.2666666806, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    internal var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.isScrollEnabled = true
        scrollView.backgroundColor = #colorLiteral(red: 0.0863005648, green: 0.0863005648, blue: 0.0863005648, alpha: 1)
        return scrollView
    }()
    
    internal var reviewView: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        return view
    }()
    
    internal var reviewImage: UIImageView = {
        let reviewimage = UIImageView()
        reviewimage.image = #imageLiteral(resourceName: "Review")
        reviewimage.contentMode = .scaleAspectFit
        reviewimage.isUserInteractionEnabled = true
        reviewimage.clipsToBounds = true
        return reviewimage
    }()
    internal var trailerImage: UIImageView = {
        let reviewimage = UIImageView()
        reviewimage.image = #imageLiteral(resourceName: "Trailers")
        reviewimage.isUserInteractionEnabled = true
        reviewimage.contentMode = .scaleAspectFit
        reviewimage.clipsToBounds = true
        return reviewimage
    }()
    internal var creditImage: UIImageView = {
        let reviewimage = UIImageView()
        reviewimage.image = #imageLiteral(resourceName: "Credit")
        reviewimage.isUserInteractionEnabled = true
        reviewimage.contentMode = .scaleAspectFit
        reviewimage.clipsToBounds = true
        return reviewimage
    }()
    internal var similarImage: UIImageView = {
        let reviewimage = UIImageView()
        reviewimage.image = #imageLiteral(resourceName: "Similars")
        reviewimage.isUserInteractionEnabled = true
        reviewimage.contentMode = .scaleAspectFit
        reviewimage.clipsToBounds = true
        return reviewimage
    }()
    
    internal var contentView: UIView = {
        let contentView = UIView()
        contentView.backgroundColor = #colorLiteral(red: 0.0863005648, green: 0.0863005648, blue: 0.0863005648, alpha: 1)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        return contentView
    }()
    
    internal var textField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.layer.cornerRadius = 8
        
        return textField
    }()
    
    internal var textField2: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.layer.cornerRadius = 8
        
        return textField
    }()
    
    
    func ExpandViewCoponent() {
        
        let frameGuide = scrollView.frameLayoutGuide
        let contentGuide = scrollView.contentLayoutGuide
        addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(backDropImage)
        contentView.addSubview(posterImage)
        contentView.addSubview(movienamelbl)
        contentView.addSubview(progressLoading)
        contentView.addSubview(stackView)
        contentView.addSubview(releaseDateLabel)
        contentView.addSubview(detailMovie)
        //contentView.addSubview(textField)
        
        
        progressLoading.addSubview(percentLabel)
        
        stackView.addArrangedSubview(reviewImage)
        stackView.addArrangedSubview(trailerImage)
        stackView.addArrangedSubview(creditImage)
        stackView.addArrangedSubview(similarImage)
        
        NSLayoutConstraint.activate([
            frameGuide.leadingAnchor.constraint(equalTo: leadingAnchor),
            frameGuide.topAnchor.constraint(equalTo: topAnchor),
            frameGuide.trailingAnchor.constraint(equalTo: trailingAnchor),
            frameGuide.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            contentGuide.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
            contentGuide.topAnchor.constraint(equalTo: contentView.topAnchor),
            contentGuide.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            contentGuide.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            contentGuide.widthAnchor.constraint(equalTo: frameGuide.widthAnchor),
        ])
        NSLayoutConstraint.activate([
            backDropImage.topAnchor.constraint(equalTo: contentView.topAnchor),
            backDropImage.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            backDropImage.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            backDropImage.heightAnchor.constraint(equalToConstant: 200)
        ])
        
        NSLayoutConstraint.activate([
            posterImage.bottomAnchor.constraint(equalTo: backDropImage.bottomAnchor,constant: -5),
            posterImage.leftAnchor.constraint(equalTo: backDropImage.leftAnchor, constant: 20),
            posterImage.heightAnchor.constraint(equalToConstant: 150),
            posterImage.widthAnchor.constraint(equalToConstant: 100),
        ])
        
        NSLayoutConstraint.activate([
            movienamelbl.topAnchor.constraint(equalTo: backDropImage.bottomAnchor, constant: 20),
            movienamelbl.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 20),
            movienamelbl.rightAnchor.constraint(equalTo: progressLoading.leftAnchor,constant: -10),
        ])
        
        NSLayoutConstraint.activate([
            progressLoading.topAnchor.constraint(equalTo: backDropImage.bottomAnchor,constant: 10),
            progressLoading.rightAnchor.constraint(equalTo: contentView.rightAnchor,constant: -20),
            progressLoading.widthAnchor.constraint(equalToConstant: 50),
            progressLoading.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            percentLabel.topAnchor.constraint(equalTo: progressLoading.topAnchor),
            percentLabel.leftAnchor.constraint(equalTo: progressLoading.leftAnchor),
            percentLabel.rightAnchor.constraint(equalTo: progressLoading.rightAnchor),
            percentLabel.bottomAnchor.constraint(equalTo: progressLoading.bottomAnchor)
        ])
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: progressLoading.bottomAnchor, constant: 10),
            stackView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            stackView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            stackView.heightAnchor.constraint(equalToConstant: 80)
        ])
        
        NSLayoutConstraint.activate([
            releaseDateLabel.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 10),
            // stackView.centerXAnchor.constraint(equalTo: centerXAnchor),
            releaseDateLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            releaseDateLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor),
        ])
        
        NSLayoutConstraint.activate([
            detailMovie.topAnchor.constraint(equalTo: releaseDateLabel.bottomAnchor,constant: 10),
            detailMovie.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            detailMovie.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            detailMovie.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
            
        ])
        
        //        NSLayoutConstraint.activate([
        //            textField.topAnchor.constraint(equalTo: detailMovie.bottomAnchor,constant: 10),
        //            textField.rightAnchor.constraint(equalTo: contentView.rightAnchor),
        //            textField.leftAnchor.constraint(equalTo: contentView.leftAnchor),
        //            textField.heightAnchor.constraint(equalToConstant: 80),
        //            textField.bottomAnchor.constraint(equalTo: contentView.bottomAnchor)
        //        ])
        
    }
    
    var delegate: ExpandViewProtocalDelegate?
    
    @objc func TappedReviews(tapGestureRecognizer: UITapGestureRecognizer)
    {
        delegate?.didSelectReviews()
        print(" select ")
    }
    @objc func TappedTrailers(tapGestureRecognizer: UITapGestureRecognizer)
    {
        delegate?.didSelectTrailers()
        print(" select ")
    }
    @objc func TappedCredits(tapGestureRecognizer: UITapGestureRecognizer)
    {
        delegate?.didSelectCredits()
        print(" select ")
    }
    @objc func TappedSimilars(tapGestureRecognizer: UITapGestureRecognizer)
    {
        delegate?.didSelectSimilars()
        print(" select ")
    }
    
    
    
    
}
