//
//  FavoritesView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 27/1/2564 BE.
//

import UIKit
import CollectionViewSlantedLayout

class FavoritesView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        FavoritesComponent()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var collectionView: UICollectionView = {
        let slantedSayout = CollectionViewSlantedLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: slantedSayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        //slantedSayout.isFirstCellExcluded = true
        
        return collectionView
    }()
    
    func FavoritesComponent() {
        addSubview(collectionView)
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
        
       
    }

}
