//
//  HomeView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 19/1/2564 BE.
//

import UIKit
protocol HomeViewProtocalDelegate {
    func backTouchUp()
    
}

class HomeView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1)
        
        homeComponent()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var backButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        button.layer.cornerRadius = 2
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(backTouchUp), for: .touchUpInside)
        return button
    }()
    
    internal var tableView: UITableView = {
       let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    func homeComponent() {
        addSubview(backButton)
        addSubview(tableView)
        NSLayoutConstraint.activate([
            backButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            backButton.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            backButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 60)
        ])
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: self.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: self.rightAnchor)
        ])
    }
    var delegate: HomeViewProtocalDelegate?
    
    @objc func backTouchUp() {
        delegate?.backTouchUp()
        print(" back ")
    }
    
    

}
