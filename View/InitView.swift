//
//  InitView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 19/1/2564 BE.
//

import UIKit
protocol InitViewProtocalDelegate {
    func didTouchUpIncreaseMaleButton(button: UIButton)
    func didTouchUpIncreaseRegister()
}

class InitView: UIView {
    
    internal var label: UILabel = {
        let label = UILabel()
        label.text = "Welcome"
        label.textColor = .black
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var textFieldId: UITextField = {
        let textField = UITextField()
        textField.placeholder = "ID"
        //textfield.textAlignment = .center
        textField.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.textColor = .black
        textField.tintColor = .blue
        //textfield.attributedPlaceholder = NSAttributedString(string: "iD", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        textField.layer.borderWidth = 2
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.layer.cornerRadius = 21
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    internal var textFieldPassword: UITextField = {
        let textField = UITextField()
        textField.placeholder = "Password"
       // textfield.textAlignment = .center
        textField.textColor = .black
        //textfield.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.gray])
        textField.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.layer.borderWidth = 2
        textField.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        textField.layer.cornerRadius = 21
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    
    internal var button: UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setTitle("Login", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), for: .normal)
        button.layer.cornerRadius = 21
        button.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        button.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        button.layer.shadowOpacity = 5
        button.addTarget(self, action: #selector(touchButton(sender:)), for: .touchUpInside)
        
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    internal var registerButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.setTitle("Register", for: .normal)
        button.setTitleColor(#colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1), for: .normal)
        button.layer.cornerRadius = 21
        button.layer.shadowColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        button.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        button.layer.shadowOpacity = 5
        button.addTarget(self, action: #selector(registerTouchButton), for: .touchUpInside)

        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        initComponent()
        
    }
    
    func initComponent() {
        addSubview(button)
        addSubview(textFieldId)
        addSubview(textFieldPassword)
        addSubview(registerButton)
        addSubview(label)
        
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: -70),
            label.widthAnchor.constraint(equalToConstant: 90),
            label.heightAnchor.constraint(equalToConstant: 50)
            
        ])
        NSLayoutConstraint.activate([
            textFieldId.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            textFieldId.topAnchor.constraint(equalTo: label.bottomAnchor,constant: 10),
            textFieldId.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
            textFieldId.heightAnchor.constraint(equalToConstant: 50)
        ])
        NSLayoutConstraint.activate([
            textFieldPassword.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            textFieldPassword.topAnchor.constraint(equalTo: textFieldId.bottomAnchor,constant: 10),
            textFieldPassword.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
            textFieldPassword.heightAnchor.constraint(equalToConstant: 50)
        ])
        
        NSLayoutConstraint.activate([
            button.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            button.topAnchor.constraint(equalTo: textFieldPassword.bottomAnchor,constant: 10),
            button.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
            button.heightAnchor.constraint(equalToConstant: 50),
        ])
        
        NSLayoutConstraint.activate([
            registerButton.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            registerButton.topAnchor.constraint(equalTo: button.bottomAnchor,constant: 10),
            registerButton.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
            registerButton.heightAnchor.constraint(equalToConstant: 50),
        ])
        
    }

    var delegate: InitViewProtocalDelegate?
    
    @objc func touchButton(sender: UIButton) {
        delegate?.didTouchUpIncreaseMaleButton(button: sender)

        
        print(" Press ")
    }
    
    @objc func registerTouchButton(){
        delegate?.didTouchUpIncreaseRegister()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
