//
//  MovieDetailView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 21/1/2564 BE.
//

import UIKit

protocol MovieDetailProtocalDelegate {
    func didSelectCell()
}
class MovieDetailView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        MovieDetailComponent()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var collectionView: UICollectionView = {
        let layout =  UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 20
        layout.minimumInteritemSpacing = 20
        
        layout.sectionInset = UIEdgeInsets(top: 0, left: 2.5, bottom: 0, right: 2.5)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout) //
        
        let size = (collectionView.frame.size.width - CGFloat(30)) / CGFloat(3)
        layout.itemSize = CGSize(width: size, height: size)
        collectionView.setCollectionViewLayout(layout, animated: true)
        
       
       // collectionView.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    func MovieDetailComponent() {
        addSubview(collectionView)
        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
           // collectionView.heightAnchor.constraint(equalToConstant: 120)
        ])
    }
    
    var delegate: MovieDetailProtocalDelegate?
    
    func detailPage() {
        
        
    }

    
    
    

}
