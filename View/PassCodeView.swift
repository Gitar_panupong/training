//
//  PassCodeView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 1/2/2564 BE.
//

import UIKit
import SmileLock

class PassCodeView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        //backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "1"))
        setupComponent()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    internal var passwordStackView: UIStackView = {
        let passcodestack = UIStackView()
        passcodestack.translatesAutoresizingMaskIntoConstraints = false
        return passcodestack
    }()

    internal var Label: UILabel = {
        let label = UILabel()
        label.text = "Enter Passcode"
        label.font = UIFont.systemFont(ofSize: 26)
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    internal var blurEffectView: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.translatesAutoresizingMaskIntoConstraints = false
//        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
//        let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
//        blurEffectView.contentView.addSubview(vibrancyView)
        return blurEffectView
    }()
    
    internal var vibrancy: UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: .dark)
        let vibrancyView: UIVisualEffectView = UIVisualEffectView(effect: UIVibrancyEffect(blurEffect: blurEffect))
        vibrancyView.translatesAutoresizingMaskIntoConstraints = false
        
        return vibrancyView
    }()
    
    func setupComponent() {
        addSubview(blurEffectView)
       
        blurEffectView.contentView.addSubview(passwordStackView)
        blurEffectView.contentView.addSubview(Label)
//        vibrancy.contentView.addSubview(passwordStackView)
//        vibrancy.contentView.addSubview(Label)
       
        NSLayoutConstraint.activate([
            blurEffectView.topAnchor.constraint(equalTo: topAnchor),
            blurEffectView.leftAnchor.constraint(equalTo: leftAnchor),
            blurEffectView.rightAnchor.constraint(equalTo: rightAnchor),
            blurEffectView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
        
//        NSLayoutConstraint.activate([
//            vibrancy.topAnchor.constraint(equalTo: blurEffectView.contentView.topAnchor),
//            vibrancy.leftAnchor.constraint(equalTo: blurEffectView.contentView.leftAnchor),
//            vibrancy.rightAnchor.constraint(equalTo: blurEffectView.contentView.rightAnchor),
//            vibrancy.bottomAnchor.constraint(equalTo: blurEffectView.contentView.bottomAnchor),
//        ])
        
        
        NSLayoutConstraint.activate([
            //Label.topAnchor.constraint(equalTo: blurEffectView.topAnchor,constant: 120),
            //Label.centerYAnchor.constraint(equalTo: blurEffectView.centerYAnchor),
            Label.leftAnchor.constraint(equalTo: blurEffectView.contentView.leftAnchor,constant: 50),
            Label.rightAnchor.constraint(equalTo: blurEffectView.contentView.rightAnchor,constant: -50),
            Label.bottomAnchor.constraint(equalTo: passwordStackView.topAnchor,constant: -50)
        ])
        
        NSLayoutConstraint.activate([
            passwordStackView.centerYAnchor.constraint(equalTo: blurEffectView.contentView.centerYAnchor),
           // passwordStackView.topAnchor.constraint(equalTo: Label.topAnchor,constant: 50),
            passwordStackView.leftAnchor.constraint(equalTo: blurEffectView.contentView.leftAnchor,constant: 50),
            passwordStackView.rightAnchor.constraint(equalTo: blurEffectView.contentView.rightAnchor,constant: -50),
            //passwordStackView.bottomAnchor.constraint(equalTo: blurEffectView.bottomAnchor,constant: -70),
        ])
        
    }
    
    
}
