//
//  RegisterView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 19/1/2564 BE.
//

import UIKit
import JJFloatingActionButton

protocol RegisterProtocalDelegate {
    func didSetButton()
}
class RegisterView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        registerComponent()
        SetButton()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    internal var tableView: UITableView = {
       var tableview = UITableView()
        tableview.translatesAutoresizingMaskIntoConstraints = false
        tableview.rowHeight = UITableView.automaticDimension
        tableview.estimatedRowHeight = 150
        
        
       return tableview
    }()
    
    internal var floatingButton: JJFloatingActionButton = {
       var floatingButton = JJFloatingActionButton()
        floatingButton.translatesAutoresizingMaskIntoConstraints = false
      return floatingButton
    }()
    
    func registerComponent(){
        addSubview(tableView)
        addSubview(floatingButton)
       
        NSLayoutConstraint.activate([
         
            tableView.topAnchor.constraint(equalTo: self.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: self.rightAnchor)
        
        ])
        
        NSLayoutConstraint.activate([
            floatingButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -30),
            floatingButton.rightAnchor.constraint(equalTo: rightAnchor,constant: -30),
        ])
        
        
        
    }
    
    var delegate: RegisterProtocalDelegate?
    
    func SetButton(){
        
        floatingButton.addItem(title: "item 1", image: UIImage(named: "fish")?.withRenderingMode(.alwaysTemplate)) { [self] item in
            delegate?.didSetButton()
        }

        floatingButton.addItem(title: "item 2", image: UIImage(named: "fish")?.withRenderingMode(.alwaysTemplate)) { item in
          // do something
        }

        floatingButton.addItem(title: "item 3", image: nil) { item in
          // do something
        }
    }
    
}
