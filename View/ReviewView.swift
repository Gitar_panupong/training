//
//  ReviewView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 26/1/2564 BE.
//

import UIKit

class ReviewView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        ReviewComponetnt()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    internal var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.rowHeight = UITableView.automaticDimension
       
//        tableView.estimatedRowHeight = UITableView.automaticDimension
 //       tableView.rowHeight = 150
//        tableView.estimatedRowHeight = 1000
//        tableView.rowHeight = UITableView.automaticDimension
        return tableView
    }()
    
    
    
    func ReviewComponetnt() {
        addSubview(tableView)
        
        NSLayoutConstraint.activate([
           // tableView.centerXAnchor.constraint(equalTo: centerXAnchor),
       //     tableView.leftAnchor.constraint(equalTo: self.leftAnchor),
         //   tableView.rightAnchor.constraint(equalTo: self.rightAnchor),
            tableView.topAnchor.constraint(equalTo: self.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
           // tableView.widthAnchor.constraint(equalTo: widthAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
}
