//
//  VisualEffectView.swift
//  Training
//
//  Created by Panupong Chaiyarut on 3/2/2564 BE.
//

import UIKit

protocol VisualEffectViewProtocalDelegate {
    func didSelectChangeTheme()
    func didSelectDarkMode()
    
}

class VisualEffectView: UIView {
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupComponent()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    internal var ChangeThemeButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.text = "ChangeTheme"
        button.backgroundColor = .systemBlue
        button.layer.cornerRadius = 60
        button.addTarget(self, action: #selector(ChangeTheme), for: .touchUpInside)
        button.theme_setTitleColor(["#FFF", "#000", "#FFF", "#000"], forState: .normal)
        button.theme_setTitleColor(["#555", "#AAA", "#555", "#AAA"], forState: .highlighted)
        button.theme_backgroundColor = ["#22446c","#EB4F38",  "#2492db", "#ECF0F1"]
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    internal var nightSwitch: UISwitch = {
        let Switch = UISwitch()
        //button.titleLabel?.text = "ChangeTheme"
        
        //Switch.layer.cornerRadius = 60
        //Switch.addTarget(self, action: #selector(ChangeDarkMode), for: .touchUpInside)
        Switch.addTarget(self, action: #selector(ChangeDarkMode(_:)), for: .valueChanged)
        Switch.translatesAutoresizingMaskIntoConstraints = false
        return Switch
    }()
    
 
    
    func setupComponent(){
        addSubview(ChangeThemeButton)
        addSubview(nightSwitch)
      
        NSLayoutConstraint.activate([
            ChangeThemeButton.centerYAnchor.constraint(equalTo: centerYAnchor),
            ChangeThemeButton.centerXAnchor.constraint(equalTo: centerXAnchor),
            ChangeThemeButton.heightAnchor.constraint(equalToConstant: 120),
            ChangeThemeButton.widthAnchor.constraint(equalToConstant: 120),
        ])
        
        NSLayoutConstraint.activate([
            nightSwitch.topAnchor.constraint(equalTo: ChangeThemeButton.bottomAnchor,constant: 50),
            nightSwitch.centerXAnchor.constraint(equalTo: centerXAnchor),
            nightSwitch.heightAnchor.constraint(equalToConstant: 120),
            nightSwitch.widthAnchor.constraint(equalToConstant: 120),
        ])
    }
    
    var delegate: VisualEffectViewProtocalDelegate?
    
    @objc func ChangeTheme() {
        delegate?.didSelectChangeTheme()
    }
    @objc func ChangeDarkMode(_ sender: UISwitch) {
        MyThemes.switchNight(isToNight: sender.isOn)
        delegate?.didSelectDarkMode()
    }
    
    

}
